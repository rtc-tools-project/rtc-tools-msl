# rtc-tools-msl

This library contains part of the Modelica Standard Library version 3.2.3
(https://github.com/modelica/ModelicaStandardLibrary/tree/maint/3.2.3)
that is used by other rtc-tools libraries.
