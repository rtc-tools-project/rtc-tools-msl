from setuptools import find_packages, setup

import versioneer


setup(
    name='rtc-tools-msl',
    version=versioneer.get_version(),
    maintainer='Deltares',
    packages=find_packages("."),
    author='Deltares',
    description="Library containing part of the Modelica Standard Library (MSL)",
    install_requires=["pymoca==0.10.*"],
    python_requires='>=3.8',
    cmdclass=versioneer.get_cmdclass(),
    entry_points={
        'rtctools.libraries.modelica': [
            'library_folder = rtctools_msl:modelica',
        ]
    },
)
